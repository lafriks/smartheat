package smartheat

import (
	"log"
	"os"
	"sync"
)

type App struct {
	Version string
}

func New(ver string) *App {
	return &App{
		Version: ver,
	}
}

func (a *App) Start() error {
	token := os.Getenv("SUPERVISOR_TOKEN")

	log.Printf("Version: %s", a.Version)
	log.Printf("Token: %s", token)

	wg := &sync.WaitGroup{}
	wg.Wait()

	return nil
}
