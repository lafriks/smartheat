package main

import (
	"codeberg.org/lafriks/smartheat/smartheat"
)

var Version string = "next"

func main() {
	app := smartheat.New(Version)
	if err := app.Start(); err != nil {
		panic(err)
	}
}
